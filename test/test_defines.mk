MACHINE = $(shell $(CC) -dumpmachine)

SRCDIR = $(realpath ../../src)
OBJDIR = $(realpath ../../output/$(MACHINE)/coverage)
INCDIR = $(realpath ../../include ../../include_priv ../include)

HEADERS = $(wildcard $(INCDIR)/*.h)
SOURCES = $(SRCDIR)/args.c $(SRCDIR)/colors.c $(SRCDIR)/config.c \
          $(SRCDIR)/connections.c $(SRCDIR)/eventloop_libevent.c \
		  $(SRCDIR)/runtime.c $(SRCDIR)/user_output.c $(SRCDIR)/dm_save_load.c 


CFLAGS += -DUNIT_TEST -Werror -Wall -Wextra -Wno-attributes\
          --std=gnu99 -g3 -Wmissing-declarations \
		  $(addprefix -I ,$(INCDIR)) -I$(OBJDIR)/.. \
		  -fkeep-inline-functions -fkeep-static-functions \
		   -Wno-format-nonliteral \
		  $(shell pkg-config --cflags cmocka) -pthread
LDFLAGS += -fkeep-inline-functions -fkeep-static-functions \
		   $(shell pkg-config --libs cmocka) -lamxc -lamxd -lamxp -lamxb -lamxo -levent
