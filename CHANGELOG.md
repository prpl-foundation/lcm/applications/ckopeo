# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]


## Release v0.1.2 - 2022-10-25(12:35:45 +0000)

## Release v0.1.1 - 2021-11-08(14:10:45 +0000)

## Release v0.1.0 - 2021-09-13(12:45:30 +0000)

## Release v0.0.0 - 2021-09-03(09:48:30 +0000)

### New

- Initial release
