# Ckopeo

[[_TOC_]]

## Introduction

Ckopeo is a CLI based on skopeo. It provides commands to copy and to delete images from remote repositories using librlyeh. It is mainly used for testing purposes without involving the other components of LCM. 


## Usage

### API

* copy : Download an image from a remote repository to a local repository.
* remove : Delete a previously downloaded image.

### CKOPEO COPY

```bash
ckopeo copy [OPTIONS] SOURCE-NAME DESTINATION-NAME
```

OPTIONS:
*   --user                       username:password to authenticate to a remote repository
*   --dest-shared-blob-dir       Location to store the downloaded blobs

SOURCE-NAME : transport://server/image-name:version
DESTINATION-NAME : oci:image-name:version

Ckopeo stores the combinations of username and password for well-known remote repositories in a config file. Therefore the informations to authenticate to a remote repository are optional if Ckopeo already has the informations.

### CKOPEO REMOVE 

```bash
ckopeo remove [OPTIONS] IMAGE-NAME VERSION
```

OPTIONS:
*   --dest-shared-blob-dir       Location to store the downloaded blobs
*   --image-location             Location of the downloaded images

### Example

```bash
user:~/test_repo$ ckopeo copy --dest-shared-blob-dir blobs docker://registry-1.docker.io/library/alpine:latest oci:library/alpine:latest

user:~/test_repo$ ckopeo remove --dest-shared-blob-dir blobs --image-location . library/alpine latest
```

Note: The image-name for registry-1.docker.io **MUST** contains the group to which the image belongs.

## Documentation

* [OCI Image Spec](https://github.com/opencontainers/image-spec)

* Some informations about [OCI Image Layout Specification](https://github.com/opencontainers/image-spec/blob/main/image-layout.md)
