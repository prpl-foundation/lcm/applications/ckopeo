/****************************************************************************
**
** Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <getopt.h>

#include "main.h"

int args_parse_remove(rlyeh_remove_data_t* data,
                      int argc,
                      char** argv) {

    int c;

    while(1) {
        int option_index = 1;

        static struct option long_options[] = {
            {"help", no_argument, 0, 0 },
            {"dest-shared-blob-dir", required_argument, 0, 1 },
            {"image-location", required_argument, 0, 2 },
            {0, 0, 0, 0 }
        };

        c = getopt_long(argc, argv, "h",
                        long_options, &option_index);
        if(c == -1) {
            break;
        }

        switch(c) {
        case 0: {
        }
        break;
        case 1:
            data->storageLocation = strdup(optarg);
            //amxc_var_add(cstring_t, GET_ARG(config, COPT_LIBDIRS), optarg);
            break;
        case 2:
            data->imageLocation = strdup(optarg);
            //amxc_var_add(cstring_t, GET_ARG(config, COPT_LIBDIRS), optarg);
            break;
        default:
            ckopeo_print_error("Argument not recognized - 0%x %c ", c, c);
        }
    }
    // there should be two positional arguments
    if((optind + 3) != argc) {
        ckopeo_print_usage(argc, argv);
        return -1;
    }
    data->name = strdup(argv[optind + 1]);
    data->version = strdup(argv[optind + 2]);
    return optind;
}

int args_parse_copy(rlyeh_copy_data_t* data,
                    int argc,
                    char** argv) {
    int c;

    while(1) {
        int option_index = 1;

        static struct option long_options[] = {
            {"help", no_argument, 0, 0 },
            {"dest-shared-blob-dir", required_argument, 0, 1 },
            {"user", required_argument, 0, 2 },
            {0, 0, 0, 0 }
        };

        c = getopt_long(argc, argv, "h",
                        long_options, &option_index);
        if(c == -1) {
            break;
        }

        switch(c) {
        case 0: {
        }
        break;
        case 1:
            data->dest_shared_blob_dir = strdup(optarg);
            //amxc_var_add(cstring_t, GET_ARG(config, COPT_LIBDIRS), optarg);
            break;
        case 2:
            data->user = strdup(optarg);
            break;
        default:
            ckopeo_print_error("Argument not recognized - 0%x %c ", c, c);
        }
    }
    // there should be two positional arguments left
    if((optind + 3) != argc) {
        ckopeo_print_usage(argc, argv);
        return -1;
    }
    data->source = strdup(argv[optind + 1]);
    data->destination = strdup(argv[optind + 2]);

    return optind;
}

void args_read_env_var(amxc_var_t* config,
                       const char* var_name,
                       const char* config_name,
                       int32_t var_type) {
    const char* env = getenv(var_name);
    amxc_var_t* coption = GET_ARG(config, config_name);
    amxc_var_t var_env;

    amxc_var_init(&var_env);

    if(env == NULL) {
        goto exit;
    }

    amxc_var_set(cstring_t, &var_env, env);

    if(coption == NULL) {
        coption = amxc_var_add_new_key(config, config_name);
        amxc_var_set_type(coption, var_type);
    }

    switch(var_type) {
    case AMXC_VAR_ID_LIST: {
        amxc_string_t* str_env = amxc_var_take(amxc_string_t, &var_env);
        amxc_llist_t parts;
        amxc_llist_init(&parts);
        amxc_string_split_to_llist(str_env, &parts, ';');
        amxc_llist_for_each(it, (&parts)) {
            amxc_string_t* part = amxc_string_from_llist_it(it);
            amxc_var_add(cstring_t, coption, amxc_string_get(part, 0));
            amxc_string_delete(&part);
        }
        amxc_llist_clean(&parts, amxc_string_list_it_free);
        amxc_string_delete(&str_env);
    }
    break;
    default: {
        amxc_var_convert(coption, &var_env, var_type);
    }
    break;
    }

exit:
    amxc_var_clean(&var_env);
    return;
}
